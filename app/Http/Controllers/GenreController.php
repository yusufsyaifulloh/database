<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GenreController extends Controller
{
    //
    public function create(){
        return view('genre.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
        ]);

        DB::table('genre')->insert([
            'nama' => $request['nama'],
        ]);

        return redirect('/genre/create');
    }
}
