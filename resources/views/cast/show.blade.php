@extends('layout.master')

@section('judul')
    Halaman Detail Cast {{$cast->nama}}
@endsection

@section('content')

<h3>{{$cast->nama}}</h3>
<p>{{'Umur : '. $cast->umur}}</p>
<p>{{$cast->bio}}</p>

@endsection