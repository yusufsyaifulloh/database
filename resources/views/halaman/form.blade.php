@extends('layout.master')

@section('judul')
    Halaman Pendaftaran
@endsection

@section('content')
    <h3>Sign Up Form</h3>
        <form  method="post" action="/home">
            @csrf
            <label>First Name</label><br><br>
            <input type="text" name="fnama"> <br><br>
            <label>Last Name</label><br><br>
            <input type="text" name="nama"> <br><br>
            <label>Gender</label><br><br>
            <input type="radio" name="G" value="male">Male<br>
            <input type="radio" name="G" value="female">Female<br>
            <input type="radio" name="G" value="other">Other<br><br>
            <label>Nationality</label><br><br>
            <select name="N"><br><br>
                <option value="1">Indonesia</option>
                <option value="2">Amerika</option>
                <option value="3">Inggris</option>
            </select><br><br>
            <label>Language Spoken</label><br><br>
            <input type="checkbox" name="LS" value="Indonesia">Bahasa Indonesia<br>
            <input type="checkbox" name="LS" value="English">English<br>
            <input type="checkbox" name="LS" value="Other">Other<br><br>
            <label>Bio</label><br><br>
            <textarea name="Bio" cols="30" rows="10"></textarea><br><br>
            <input type="submit" value="Sign Up">
        </form>
@endsection