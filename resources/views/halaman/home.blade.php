@extends('layout.master')

@section('judul')
    SELAMAT DATANG!
@endsection

@section('content')
    <h3>Halo, {{$nama}}!</h3>
    <h3>Terima kasih telah telah bergabung di Website Kami. Media Belajar kita bersama!</h3>
@endsection